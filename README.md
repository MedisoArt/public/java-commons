# java-commons

Common parts for our Java projects.

## Checkstyle rules
Our modified set of rules for [Checkstyle](http://checkstyle.sourceforge.net/) is in the file `checkstyle-rules.xml`.

## PMD rules
Our set of rules for [PMD](https://pmd.github.io/) is in the file `pmd-rules.xml`.

## Apache Ant build file
Remote [Apache Ant](https://ant.apache.org/) build file `build.xml` with common targets.
